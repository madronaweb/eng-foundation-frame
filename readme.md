# Erin Norman Gardens Foundation Template

This template is jacked up and not ready for direct distribution to server after a build.

Live site needs PHP extensions and additions. Links within foundation site are .php so are useless in development.

Foundation template lives locally at /foundation-erin-norman-gardens-fixed
Up to date PHP site lives at erin-norman-gardens

To go live:

1. Keep directory structure especially perch as is on server
2. foundation build
3. change extensions of updated pages .php
4. backup old .php .css and .js
4. upload new .php .css and .js
5. cross fingers
6. copy new files to local php directory
7. add, commit, and push files to git